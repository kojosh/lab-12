#include <string>
#include <iostream>

using namespace std;

class FootballPlayer {
private:
    string name;

public:

    FootballPlayer(const string &newName) {
        name = newName;
    }

    string getName() const {
        return name;
    }

};

class FootballPlayerList {

private:
    FootballPlayer **list;
    int size;
    int maxSize;

public:

    FootballPlayerList() : FootballPlayerList(100) {}

    FootballPlayerList(int newMaxSize) {
        maxSize = newMaxSize;
        list = new FootballPlayer*[maxSize];
        size = 0;
    }

    ~FootballPlayerList() {
        for (int i = 0; i < size; i++) {
            delete list[i];
        }
        delete [] list;
    }

    // FootballPlayerList will take ownership of the pointer and delete it
    // on destruction.
    void add(FootballPlayer *player) {
        if (size == maxSize) {
            return;
        }

        list[size++] = player;
    }

    FootballPlayer *getElement(int index) const {
        if (index >= size) {
            return nullptr;
        }
        return list[index];
    }
    
    int getSize() const {
        return size;
    }

    void replace(int index, FootballPlayer *player) {
        if (index >= size) {
            return;
        }
        delete list[index];
        list[index] = player;
    }

    void print(FootballPlayerList &players) {
        for (int i = 0; i < players.getSize(); i++) {
            cout << players.getElement(i)->getName() << endl;
        }
    }

    void resize(int);

};

int main()
{
    FootballPlayerList players;

    players.add(new FootballPlayer("Mahomes"));
    players.add(new FootballPlayer("Kelce"));
    players.add(new FootballPlayer("Pacheco"));

    players.print(players);
        
    return 0;
}

void FootballPlayerList::resize(int newMax) {
    FootballPlayer **tempList = new FootballPlayer *[newMax];
    // newMax > size: new size will be larger, there will be unused pointers at the end
    if(newMax > size) {
        for(int i = 0; i < size; i++) {
            tempList[i] = list[i];
        }
    }
    // newMax < size: some pointers will go away, remember to delete them first
    if(newMax < size) {
        int i;
        for(i = 0; i < newMax; i++) {
            tempList[i] = list[i];
        }
        while(i < size) {
            delete list[i];
            i++;
        }
        size = newMax;
    }
    // newMax == size: nothing happens
    maxSize = newMax;
    delete [] list;
    list = tempList;
}